# RESERVOIR SIMULATION THROUGH OPEN SOURCE SOFTWARE #

### About this repository ###

* The repository contains code, simulator input files, and other relevant material related to the book "RESERVOIR SIMULATION THROUGH OPEN SOURCE SOFTWARE"

### Contents ###

* flow: Input files for the reservoir simulator OPM-Flow
* python: Python code

### Repository owners ###

* Carl Fredrik Berg
* Per Arne Slotte