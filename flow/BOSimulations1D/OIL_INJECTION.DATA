-- This file is made available under the Open Database License:
-- http://opendatacommons.org/licenses/odbl/1.0/.

-- File provided by NTNU

-- This file is support material to the book
-- "Reservoir simulation through open source software"
-- by Carl Fredrik Berg and Per Arne Slotte

-- Injection of oil (Rs=0) into an undersaturated oil reservoir
-- 1D model with length 100 grid cells and length 1000m

RUNSPEC
-- --------------------------------------------------

TITLE
  Oil injection

NOECHO

DIMENS
   100 1 1 /

-- The number of equilibration regions is inferred
-- from the EQLDIMS keyword.
EQLDIMS
 2 /

-- Table sizes:
--
TABDIMS
--     1: Number of SATNUM regions
--     2: Number of PVT regions
--     3: Max number of entries in a relperm table
--     4: Max number of entries in a PVT table
--     5: Number of FIP regions
--     6: Max number of Rs values in a PVT table
--     7: Max number of Rv values in a PVT table
-- Item #:1 2  3   4  5  6   7
          1 1 100 100 1 100 100 
/

OIL
GAS
WATER
DISGAS
VAPOIL

METRIC

START
   1 'JAN' 2022 /

WELLDIMS
-- Item 1: maximum number of wells in the model
--         - there are two wells in the problem;
--           injector and producer
-- Item 2: maximum number of grid blocks connected
-- to any one well
--         - must be one as the wells are located
--           at specific grid blocks
-- Item 3: maximum number of groups in the model
--         - we are dealing with only one 'group'
-- Item 4: maximum number of wells in any one group
--         - there must be two wells in a group as
--           there are two wells in total
   2 1 1 2 /

UNIFOUT

INCLUDE
   'GRID.INC' /


PROPS
-- ------------------------------------------------

INCLUDE
   'NORNE_SIMPLIFIED_PVT.INC' /


INCLUDE
   'NORNE_SIMPLIFIED_SCAL.INC' /


REGIONS
-- -------------------------------------------------

EQLNUM
 10*1 90*2
/

SOLUTION
-- -------------------------------------------------

EQUIL
-- Item 1: datum depth (m)
--         - Set at top of reservoir
-- Item 2: pressure at datum depth (bar)
-- Item 3: depth of water-oil contact (m)
--         - chosen to be below the
-- Item 4: oil-water capillary pressure at the
-- water oil contact (bar)
-- Item 5: depth of gas-oil contact (m)
--         - chosen to be directly above the
--           reservoir
-- Item 6: gas-oil capillary pressure at gas-oil
-- contact (bar)
--         - given to be 0 in Odeh's paper
-- Item 7: RSVD-table
-- Item 8: RVVD-table
-- Item 9: Set to 0 as this is the only value
-- supported by OPM
-- Item #: 1  2   3   4   5   6 7 8 9
        1000 250 1500 0  500  0 1 1 0 /
        1000 250 1500 0  500  0 1 1 0 /

RSVD
-- Dissolved gas is initially constant with depth
-- through the reservoir.
-- The bubble point pressure for an oil with Rs=90.0
-- is lower than the initial reservoir pressure p=250
500  0.0
1500 0.0 /
500  90.0
1500 90.0 /

RVVD
 0    0
 500 0.0 /
 0    0
 500 0.0 /

-- Write to the reatart file for visualization
-- This keyword should be placed in the SOLUTION section
-- to get a report at timestep=0
--
-- Extra properties (ALLPROPS) are activated by the
-- command line option -enable-opm-rst-file=1
--
RPTRST
        'BASIC=1' 'ALLPROPS' /


SUMMARY
-- -------------------------------------------------

INCLUDE
  'SUMMARY.INC' /

SCHEDULE
-- -------------------------------------------------

RPTSCHED
 'FIP=1' /


-- If no resolution the two following
-- lines must be added:
--DRSDT
--  0.0  /
-- if DRSDT is set to 0, RS cannot rise and more
-- gas does not dissolve in undersaturated oil

INCLUDE
   'WELLS.INC' /


-- Controls for injectors
--
WCONINJE
--      1: Name
--      2: Type
--      3: Open--stopped--shut
--      4: Preferred control mode
--      5: Max surface flow rate
--      6: Max reservoir flow rate
--      7: Max BHP
--      8: Max THP
--      9: Lift (VFP) table
--     10: Rv in gas or Rs in oil 
--     
-- Item #:1      2         3       4      5   6    7    8-9 10
        'INJ'   'OIL'   'OPEN'  'RESV'  1*  10.0  390.0 2*  0.0 /
/

-- Tuning parameters are activated by
-- flow --enable-tuning=1
-- 
TUNING
-- 1: Initial time step
-- 2: Max time step
--
-- Item #:  1    2
           0.1  7.0 /
 /
 /

TSTEP
 100*30.0 /


END
