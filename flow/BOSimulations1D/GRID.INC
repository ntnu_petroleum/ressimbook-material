-- This file is made available under the Open Database License:
-- http://opendatacommons.org/licenses/odbl/1.0/.

--
-- 1D model
--
-- Depth: 1000m
-- Lx   : 1000m
-- Ly   : 10m
-- Lz   : 10m
-- Nx   : 100
-- Ny   : 1
-- Nz   : 1

--
GRID
-- ------------------------------------------------

INIT

DX 
-- There are in total 100 cells with length 10m in
-- the x-direction	
   	100*10.0 /
DY
-- There are in total 100 cells with length 10m in
-- the y-direction	
	100*10.0 /
DZ
-- The layer is 10 m thick
	100*10.0 /

TOPS
-- The depth of the top of each grid block
	100*1000 /

PORO
-- Constant porosity of 0.25 throughout all 100 grid
-- cells
   	100*0.25 /

PERMX
-- Constant permeability of 500mD
	100*500 /

PERMY
-- Equal to PERMX
	100*500 /

PERMZ
-- Equal to PERMX
	100*500 /
