
import matplotlib.pyplot as plt

from resdata import summary

summaryData90 = summary.Summary('CENTER90.UNSMRY')

timeInDays90 = summaryData90.get_days()
productionRate90 =  summaryData90.numpy_vector('FWPR')
productionTotal90 =  summaryData90.numpy_vector('FWPT')

# ------------------------------

summaryData30 = summary.Summary('CENTER30.UNSMRY')

timeInDays30 = summaryData30.get_days()
productionRate30 =  summaryData30.numpy_vector('FWPR')
productionTotal30 =  summaryData30.numpy_vector('FWPT')

# ------------------------------

summaryData10 = summary.Summary('CENTER10.UNSMRY')

timeInDays10 = summaryData10.get_days()
productionRate10 =  summaryData10.numpy_vector('FWPR')
productionTotal10 =  summaryData10.numpy_vector('FWPT')


# ------------------------------

summaryData270 = summary.Summary('CENTER270.UNSMRY')

timeInDays270 = summaryData270.get_days()
productionRate270 =  summaryData270.numpy_vector('FWPR')
productionTotal270 =  summaryData270.numpy_vector('FWPT')


# -------------------------------
# Compare Production:

print(f'Rates at {timeInDays90[-1]} days:')
print(f'270X270: {productionRate270[-1]}')
print(f'90X90  : {productionRate90[-1]}')
print(f'30X30  : {productionRate30[-1]}')
print(f'10X10  : {productionRate10[-1]}')

print(f'Total production at {timeInDays90[-1]} days:')
print(f'270X270: {productionTotal270[-1]}')
print(f'90X90  : {productionTotal90[-1]}')
print(f'30X30  : {productionTotal30[-1]}')
print(f'10X10  : {productionTotal10[-1]}')

# -------------------------------
# Create plots:

fig,ax = plt.subplots()

ax.plot(timeInDays270,productionRate270,color='green')
ax.plot(timeInDays90,productionRate90,color='red')
ax.plot(timeInDays30,productionRate30,color='blue')
ax.plot(timeInDays10,productionRate10,color='black')

ax.set_xlim(0.0,3.0)

plt.show()
#plt.savefig('centerplot_short.pdf')

# -------------------------------
# Create plots:

fig,ax = plt.subplots()

ax.plot(timeInDays270,productionRate270,color='green')
ax.plot(timeInDays90,productionRate90,color='red')
ax.plot(timeInDays30,productionRate30,color='blue')
ax.plot(timeInDays10,productionRate10,color='black')

ax.set_xlim(0.0,timeInDays90[-1])

plt.show()
#plt.savefig('centerplot.pdf')

