#!/usr/bin/env python3
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################
#
# A script to generarte SWOF tables for the
# Flow reservoir simulator.
# Carl Fredrik Berg, carl.f.berg@ntnu.no, 2019
#

import argparse

# ****************** INPUT *********************
parser = argparse.ArgumentParser(prog='flowPlot.py',description='A script to plot reservoir simulation results from the Flow reservoir simulator.')
parser.add_argument("-corey", type=str,help="Corey parameters, on the format Nw, No, Krwo.")
parser.add_argument("-let", type=str,help="LET parameters Lw,Ew,Tw,Lo,Eo,To,Krwo.")
parser.add_argument("-swirr", type=float,default=0.2,help="The irreduicble water saturation. Default value is 0.2.")
parser.add_argument("-sorw", type=float,default=0.2,help="The residual oil saturation. Default value is 0.2.")
parser.add_argument("-points", type=int,default=20,help="The number of points in the table. Default value is 20.")
parser.add_argument("-save",type=str,help="The file name to save the plot. Default is to save to the file SWOF.INC")

args = parser.parse_args()

import numpy as np

def normSw(fSw,fSwirr,fSorw):
    return (fSw-fSwirr)/(1.0-fSorw-fSwirr)

def coreyWater(fSw,fNw,fKrwo):
    return fKrwo*fSw**fNw

def coreyOil(fSw,fNo):
    return (1-fSw)**fNo

def letWater(fSw,fL,fE,fT,fKrwo):
    return fKrwo*fSw**fL/(fSw**fL+fE*(1-fSw)**fT)

def letOil(fSw,fL,fE,fT):
    return (1-fSw)**fL/((1-fSw)**fL+fE*fSw**fT)

fSwirr=args.swirr
fSorw=args.sorw

afSw=np.linspace(fSwirr,1.0-fSorw,args.points)

if args.corey:
	fNw=float(args.corey.split(',')[0])
	fNo=float(args.corey.split(',')[1])
	fKrwo=float(args.corey.split(',')[2])

if args.let:
	fLw=float(args.corey.split(',')[0])
	fEw=float(args.corey.split(',')[1])
	fTw=float(args.corey.split(',')[2])
	fLo=float(args.corey.split(',')[3])
	fEo=float(args.corey.split(',')[4])
	fTo=float(args.corey.split(',')[5])
	fKrwo=float(args.corey.split(',')[6])

afNormSw=normSw(afSw,fSwirr,fSorw)
strSwof='SWOF \n'
if args.corey:
	afKrw=coreyWater(afNormSw,fNw,fKrwo)
	afKro=coreyOil(afNormSw,fNo)
if args.let:
	afKrw=letWater(fSw,fLw,fEw,fTw,fKrwo)
	afKro=letOil(fSw,fLw,fEw,fTw)

for ii in range(0,len(afSw)-1):
	strSwof+=' '+str(round(afSw[ii],5))+' '+str(round(afKrw[ii],5))+' '+str(round(afKro[ii],5))+' 0.0 \n'
strSwof+=' '+str(afSw[len(afSw)-1])+' '+str(round(afKrw[len(afSw)-1],5))+' '+str(round(afKro[len(afSw)-1],5))+' 0.0 / \n'

if args.save:
	strFileName=args.save
else:
	strFileName='SWOF.INC'

hOutFile=open(strFileName,'w')
hOutFile.write(strSwof)
hOutFile.close()
