# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

# -------------------------------------------------------------------

# File provided by NTNU

# This file is support material to the book
# "Reservoir simulation through open source software"
# by Carl Fredrik Berg and Per Arne Slotte

# -------------------------------------------------------------------


# Corey relative permeability Oil/Water

def normSw(Sw,Swirr,Sorw):
    return (Sw-Swirr)/(1.0-Sorw-Swirr)


class coreyWater:
    '''
    Water relative permeability
    '''
    def __init__(self,Nw,Krwo,Swirr,Sorw):
        '''
        Args:
           Nw:    Exponent
           Krwo:  Relperm at 1-S_{orw}
           Swirr: S_{wi}
           Sorw:  S_{orw}
        '''
        self.Nw   = Nw
        self.Krwo = Krwo
        self.Swirr = Swirr
        self.Sorw = Sorw

    def __call__(self,Sw):
        nSw = normSw(Sw,self.Swirr,self.Sorw)
        return  self.Krwo*nSw**self.Nw


class coreyOil:

    def __init__(self,No,Swirr,Sorw):
        '''
        Args:
           No:    Exponent
           Swirr: S_{wi}
           Sorw:  S_{orw}
        '''
        self.No = No
        self.Swirr = Swirr
        self.Sorw = Sorw


    def __call__(self,Sw):
        nSw = normSw(Sw,self.Swirr,self.Sorw)
        return (1.0-nSw)**self.No

    
