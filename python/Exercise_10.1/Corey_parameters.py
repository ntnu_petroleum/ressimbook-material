# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

# -------------------------------------------------------------------

# File provided by NTNU

# This file is support material to the book
# "Reservoir simulation through open source software"
# by Carl Fredrik Berg and Per Arne Slotte

# -------------------------------------------------------------------

#
# Example script for Exercise 10.1
#

import numpy as np
import matplotlib.pyplot as plt
from IMPES1D import  Simulator1DIMPES
from conversion import daysToSeconds,secondsToDays
from corey import coreyWater,coreyOil

N = 100

maxtime        = daysToSeconds(400)
reportInterval = daysToSeconds(100)

fig,ax = plt.subplots()

cmap = plt.get_cmap('gnuplot')


# ----------------------------
# Plot with default parameters:

simulator = Simulator1DIMPES(N,200.0)


xValues = np.arange(simulator.deltaX/2,simulator.length,simulator.deltaX)

while simulator.time<maxtime:
    simulator.simulateTo(simulator.time + reportInterval)
    ax.plot(xValues,simulator.saturation,linestyle="-",color=cmap(1-simulator.time/maxtime))

# -------------------------------------------    
# Plot with changed corey exponent for water:

simulator = Simulator1DIMPES(N,200.0)
simulator.relpermWater = coreyWater(3.0,0.4,0.2,0.2)
#simulator.mobilityWeighting = 0.1

while simulator.time<maxtime:
    simulator.simulateTo(simulator.time + reportInterval)
    ax.plot(xValues,simulator.saturation,linestyle="--",color=cmap(1-simulator.time/maxtime))

    
# -------------------------------------------    
# Plot with changed corey exponent for oil:

simulator = Simulator1DIMPES(N,200.0)
simulator.relpermOil   = coreyOil(3.0,0.2,0.2)

while simulator.time<maxtime:
    simulator.simulateTo(simulator.time + reportInterval)
    ax.plot(xValues,simulator.saturation,linestyle="-.",color=cmap(1-simulator.time/maxtime))

    
plt.savefig('Corey_parameters.pdf')

    

