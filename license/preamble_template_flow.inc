-- This file is made available under the Open Database License:
-- http://opendatacommons.org/licenses/odbl/1.0/.

-- IMPORTANT: If files are created or adapted based on work from OPM
--            or other sources an attribution has to be added here

-- File provided by NTNU

-- This file is support material to the book
-- "Reservoir simulation through open source software"
-- by Carl Fredrik Berg and Per Arne Slotte




